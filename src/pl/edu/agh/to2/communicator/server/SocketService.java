package pl.edu.agh.to2.communicator.server;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


/**
 * Created by Maciej on 2015-11-18.
 */

public class SocketService {

    private int socket;
    private ServerSocket serverSocket;
    private ArrayList<Socket> clients;

    public SocketService(int socket) {
        this.socket = socket;
    }

    public void listenSocket() throws IOException {

        // create server socket
        serverSocket = new ServerSocket(socket);
        clients = new ArrayList<Socket>();

        // waits for the connection and create network socket
        while(true){
            Socket client;
            client = serverSocket.accept();
            clients.add(client);

            System.out.println("Jest połączenie: " + client);

            // receive thread creating
            new ServerReceiver(client, clients).start();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        // close connection
        serverSocket.close();

        super.finalize();
    }
}
