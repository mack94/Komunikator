package pl.edu.agh.to2.communicator.server;

import java.io.IOException;

/**
 * Created by Maciej on 2015-11-17.
 */
public class Server {

    private static int socket;
    private static SocketService socketService;

    public static void main(String[] args){
        socket = 3333;
        socketService = new SocketService(socket);


        try {
            socketService.listenSocket();
        } catch (IOException e) {
            System.out.println(e.toString());
            System.exit(-1);
        }

    }

}
