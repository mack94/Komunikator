package pl.edu.agh.to2.communicator.server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Maciej on 2015-11-24.
 */
public class ServerReceiver extends Thread{

    private Socket client;
    private ArrayList<Socket> clients;
    private BufferedReader clientReader;
    private ArrayList<PrintWriter> clientsSender;

    public ServerReceiver(Socket client, ArrayList<Socket> clients) throws IOException {
        this.client = client;
        this.clients = clients;
        this.clientReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        clientsSender = new ArrayList<PrintWriter>();
        for(Socket cl : clients){
            clientsSender.add(new PrintWriter(cl.getOutputStream(), true));
        }
    }

    public void run() {
        while (true){
            String line = null;
            try {
                line = clientReader.readLine();
                for(PrintWriter clientSender : clientsSender){
                    clientSender.println(line);
                }
                System.out.println("<"+ client + "> " + line);
            } catch (IOException e) {
                clients.remove(client);
                return;
            }
        }
    }

    public void finalize() {
        // close connection
        try {
            clientReader.close();
            for(PrintWriter clientSender : clientsSender){
                clientSender.close();
            }
            client.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
