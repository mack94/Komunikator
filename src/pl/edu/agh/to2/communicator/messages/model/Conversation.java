package pl.edu.agh.to2.communicator.messages.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Maciej on 2015-11-17.
 */
public class Conversation {

    private long conversationId;
    private Set<Message> messageSet = new HashSet<Message>();

    public Conversation() {
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    public Set<Message> getMessageSet() {
        return messageSet;
    }

    public void setMessageSet(Set<Message> messageSet) {
        this.messageSet = messageSet;
    }

    public void addMessageToSet(Message message) {
        this.messageSet.add(message);
    }
}
