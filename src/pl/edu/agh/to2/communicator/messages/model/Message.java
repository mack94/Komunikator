package pl.edu.agh.to2.communicator.messages.model;

/**
 * Created by Maciej on 2015-11-17.
 */

public class Message {

    private long messageId;
    private String content;

    public Message(String content) {
        this.content = content;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
