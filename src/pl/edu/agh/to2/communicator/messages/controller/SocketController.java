package pl.edu.agh.to2.communicator.messages.controller;

import javafx.scene.control.*;

import java.io.*;
import java.net.Socket;
import java.util.Properties;

/**
 * Created by Maciej on 2015-11-18.
 */
public class SocketController {

    private ClientReceiver clientReceiver;
    private ClientSender clientSender;
    private TextArea receivedMessageTextArea;
    private Socket server;

    public SocketController(TextArea receivedMessageTextArea) {

        this.receivedMessageTextArea = receivedMessageTextArea;

        try {
            listenSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void listenSocket() throws IOException {

        Properties properties = new Properties();
        LoadProperties(properties);

        CreateConnectionWithTheServer(properties);

        // receiving thread creating
        clientReceiver = new ClientReceiver(server, receivedMessageTextArea);
        clientReceiver.start();

        // sending thread creating
        clientSender = new ClientSender(server);
        clientSender.start();

    }

    private void CreateConnectionWithTheServer(Properties properties) throws IOException {
        String serverAddress = properties.getProperty("serverAddress");
        int serverPort = Integer.parseInt(properties.getProperty("serverPort"));

        server = new Socket(serverAddress, serverPort);
        System.out.println("<Nawiązałem połączenie z: " + server);

    }

    private void LoadProperties(Properties properties) throws IOException {

        InputStream inputStream = new FileInputStream("config.properties");
        properties.load(inputStream);

    }

    public void sendMessageActionEvent(TextField textField){
        clientSender.sendMessageActionEvent(textField);
    }

    protected void finalize() {
        try {
            server.close();
        } catch (IOException e) {
            System.out.println("Error during finalization");
            System.exit(-1);
        }
    }
}
