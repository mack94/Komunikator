package pl.edu.agh.to2.communicator.messages.controller;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pl.edu.agh.to2.communicator.messages.model.Message;

import java.awt.Button;
import java.net.URL;
import java.util.ResourceBundle;

public class DisplayController implements Initializable{

    private SocketController socketController;
    private MessageController messageController;


    @FXML
    Button saveButton;

    @FXML
    TextField newMessageTextField = new TextField();

    @FXML
    TextArea receivedMessageTextArea = new TextArea();

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        socketController = new SocketController(receivedMessageTextArea);
        messageController = new MessageController();
    }

    @FXML
    public void sendOnMouseClicked(){

        Message message = new Message(newMessageTextField.getText());
        messageController.saveMessage(message);

        socketController.sendMessageActionEvent(newMessageTextField);
    }

    @FXML
    public void sendMessageIfEnter(){
        sendOnMouseClicked();
    }

    @FXML
    public void refreshOnMouseClicked(){
//        socketController.receiveMessageActionEvent(receivedMessageTextArea);
    }

}
