package pl.edu.agh.to2.communicator.messages.controller;

import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Date;

/**
 * Created by Maciej on 2015-11-24.
 */
public class ClientReceiver extends Thread {

    private Socket server;
    private BufferedReader serverReader;
    private TextArea receivedMessageTextArea;

    public ClientReceiver(Socket server, TextArea receivedMessageTextArea) throws IOException {
        this.server = server;
        this.receivedMessageTextArea = receivedMessageTextArea;
        this.serverReader = new BufferedReader(new InputStreamReader(server.getInputStream()));
        System.out.println("Utworzono ClientReceiver");

    }

    public void run() {
        try {
            receiveMessageActionEvent();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void receiveMessageActionEvent() throws InterruptedException {

        String line = null;
        while(true){
            try {
                if(serverReader.ready()){
                    Date actualDate = new Date();
                    line = serverReader.readLine();
                    String oldText = receivedMessageTextArea.getText();

                    receivedMessageTextArea.setText(oldText + "\n" + actualDate.toString() + ": " + line);
                    ScrollTextArea();
                }

            } catch (Exception e) {
                return;
            }
            Thread.sleep(1000);
        }
    }

    private void ScrollTextArea() {
        receivedMessageTextArea.appendText("");
    }

    public void finalize() {
        try {
            serverReader.close();
            server.close();

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
