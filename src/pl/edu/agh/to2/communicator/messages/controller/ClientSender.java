package pl.edu.agh.to2.communicator.messages.controller;

import javafx.scene.control.TextField;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Maciej on 2015-11-24.
 */
public class ClientSender extends Thread{

    private Socket server;
    private PrintWriter clientWriter;

    public ClientSender(Socket server) throws IOException {
        this.server = server;
        this.clientWriter = new PrintWriter(server.getOutputStream(), true);
    }

    public void run(){

    }

    public void sendMessageActionEvent(TextField textField){
        String text = textField.getText();
        clientWriter.println(text);
        textField.setText(new String(""));
    }

    public void finalize() throws Throwable {
        clientWriter.close();
        server.close();
        super.finalize();
    }

}
